# ************************************************************************
# Parameter 1: Cubename
# Parameter 2: "plain" "main" or "oop"
# Parameter 3: "icallgraph" or "no"
# Parameter 4: "impact" or "no"
# Parameter 5: (optional) Portnumber of Server
# ************************************************************************

bash "$ITP/script/olap/build.sh" project main icallgraph impact 4080
